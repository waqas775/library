<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BookController extends Controller
{
    /**
     * @Route("/", name="book_list")
     */
    public function listAction()
    {
        $books = $this->getDoctrine()->getRepository('AppBundle:Book')->findAll();
        return $this->render('library/index.html.twig', array(
            'books' => $books
        ));
    }

    /**
     * @Route("/book/{nameOfTheBook}", name="book_details")
     */
    public function detailAction($nameOfTheBook)
    {
        $book = $this->getDoctrine()->getRepository('AppBundle:Book')->findOneBy(['name' => $nameOfTheBook]);
        return $this->render('library/detail.html.twig', array(
            'book' => $book
        ));
    }

}
