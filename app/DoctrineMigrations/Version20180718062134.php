<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180718062134 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE book (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, release_date DATE NOT NULL, length INT NOT NULL, user_readable TINYINT(1) NOT NULL, admin_readable TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE book_genre (book_id INT NOT NULL, genre_id INT NOT NULL, INDEX IDX_8D92268116A2B381 (book_id), INDEX IDX_8D9226814296D31F (genre_id), PRIMARY KEY(book_id, genre_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genre (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_957A6479C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE book_genre ADD CONSTRAINT FK_8D92268116A2B381 FOREIGN KEY (book_id) REFERENCES book (id)');
        $this->addSql('ALTER TABLE book_genre ADD CONSTRAINT FK_8D9226814296D31F FOREIGN KEY (genre_id) REFERENCES genre (id)');

        $this->addSql('INSERT INTO `book` (`id`, `name`, `release_date`, `length`, `user_readable`, `admin_readable`) VALUES (1, \'Doctor With Big Eyes\', \'2016-02-01\', \'200\', \'1\', \'1\');');
        $this->addSql('INSERT INTO `book` (`id`, `name`, `release_date`, `length`, `user_readable`, `admin_readable`) VALUES (2, \'Hunger Of My Town\', \'2016-02-05\', \'10\', \'1\', \'1\');');
        $this->addSql('INSERT INTO `book` (`id`, `name`, `release_date`, `length`, `user_readable`, `admin_readable`) VALUES (3, \'Colleagues And Demons\', \'2015-06-04\', \'30\', \'1\', \'1\');');
        $this->addSql('INSERT INTO `book` (`id`, `name`, `release_date`, `length`, `user_readable`, `admin_readable`) VALUES (4, \'Humans In The Library\', \'1982-06-15\', \'600\', \'0\', \'1\');');
        $this->addSql('INSERT INTO `book` (`id`, `name`, `release_date`, `length`, `user_readable`, `admin_readable`) VALUES (5, \'Founders Of Evil\', \'1530-08-30\', \'900\', \'1\', \'1\');');
        $this->addSql('INSERT INTO `book` (`id`, `name`, `release_date`, `length`, `user_readable`, `admin_readable`) VALUES (6, \'Ancestor With Horns\', \'2019-10-10\', \'1000\', \'1\', \'1\');');
        $this->addSql('INSERT INTO `book` (`id`, `name`, `release_date`, `length`, `user_readable`, `admin_readable`) VALUES (7, \'Age Of The Light\', \'1923-06-12\', \'234\', \'1\', \'1\');');
        $this->addSql('INSERT INTO `book` (`id`, `name`, `release_date`, `length`, `user_readable`, `admin_readable`) VALUES (8, \'Learning With The River\', \'1965-02-02\', \'200\', \'1\', \'1\');');
        $this->addSql('INSERT INTO `book` (`id`, `name`, `release_date`, `length`, `user_readable`, `admin_readable`) VALUES (9, \'Lord And Buffoon\', \'2001-09-07\', \'240\', \'1\', \'1\');');

        //insert data in table genre
        $this->addSql('INSERT INTO `genre` (`id`, `name`) VALUES (1, \'Police\');');
        $this->addSql('INSERT INTO `genre` (`id`, `name`) VALUES (2, \'Comedy\');');
        $this->addSql('INSERT INTO `genre` (`id`, `name`) VALUES (3, \'Drama\');');
        $this->addSql('INSERT INTO `genre` (`id`, `name`) VALUES (4, \'Non-fiction\');');
        $this->addSql('INSERT INTO `genre` (`id`, `name`) VALUES (5, \'Horror\');');
        $this->addSql('INSERT INTO `genre` (`id`, `name`) VALUES (6, \'Tragedy\');');
        $this->addSql('INSERT INTO `genre` (`id`, `name`) VALUES (7, \'Children\');');
        $this->addSql('INSERT INTO `genre` (`id`, `name`) VALUES (8, \'Fiction\');');
        $this->addSql('INSERT INTO `genre` (`id`, `name`) VALUES (9, \'Satire\');');

        //insert data in table book_genre
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (1, 1);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (2, 2);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (3, 3);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (4, 4);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (4, 5);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (5, 3);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (6, 3);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (7, 6);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (8, 7);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (8, 8);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (9, 5);');
        $this->addSql('INSERT INTO `book_genre` (`book_id`, `genre_id`) VALUES (9, 9);');

        $this->addSql('INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES (NULL, \'user\', \'user\', \'user@test.com\', \'user@test.com\', 1, NULL, \'$2y$13$CL5jj88x5yXaN..lQWaJCu0i8Fd96tTxLdIdLPMm8/wcO4NwCktBK\', NULL, NULL, NULL, \'a:0:{}\');');
        $this->addSql('INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES (NULL, \'admin\', \'admin\', \'admin@test.com\', \'admin@test.com\', 1, NULL, \'$2y$13$/k9nIFFMCnuzpsqNofJMBuk8ws5qJCjzR8wOt.dL5wUPBRmod1sjK\', NULL, NULL, NULL, \'a:1:{i:0;s:10:"ROLE_ADMIN";}\');');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE book_genre DROP FOREIGN KEY FK_8D92268116A2B381');
        $this->addSql('ALTER TABLE book_genre DROP FOREIGN KEY FK_8D9226814296D31F');
        $this->addSql('DROP TABLE book');
        $this->addSql('DROP TABLE book_genre');
        $this->addSql('DROP TABLE genre');
        $this->addSql('DROP TABLE fos_user');
    }
}
